package com.openbravo.pos.payment;

import java.util.logging.Logger;

import com.openbravo.pos.payment.DevicePayment;
import com.openbravo.pos.service.HardwareConfig;
import com.openbravo.pos.service.PaymentService;

public class DeviceForNIPayment implements PaymentService{
	
	public static final Logger log = Logger.getLogger(DeviceForNIPayment.class.getName());

	@Override
	public DevicePayment getPayment(String arg0, HardwareConfig arg1) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("NI payment device working");
		log.info("Entered DeviceForNIPayment class");
		DevicePayment test = new NIPaymentDevice();
		return test;
	}
}