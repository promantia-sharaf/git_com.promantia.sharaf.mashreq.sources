package com.openbravo.pos.payment;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

import javax.swing.JComponent;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.openbravo.pos.payment.DevicePayment;
import com.openbravo.pos.payment.PaymentRequest;
import com.openbravo.pos.payment.PaymentResult;
import com.openbravo.poshw.AppConfig;

import ECRWrapper.DLLLoader;
import ECRWrapper.TxnInput;
import ECRWrapper.TxnOutput;

public class NIPaymentDevice implements DevicePayment  {
	
	public static Logger log = Logger.getLogger(NIPaymentDevice.class.getName());
	    
	public PaymentResult executeStub(PaymentRequest params, PaymentResult pr) {
		
		//new DecimalFormat("#").format(amt)
		String[] args = {};
		AppConfig prop = new AppConfig(args);
		prop.load();
		log.info("inside execute stub method");
		String PortName = prop.getProperty("NIPD_PortName");
		String Baudrate = prop.getProperty("NIPD_Baudrate");
		String Timeout = prop.getProperty("NIPD_Timeout");
		String TxnTimeout = prop.getProperty("NIPD_TxnTimeout");
		String UploadTimeout = prop.getProperty("NIPD_UploadTimeout");
		String TracePath = prop.getProperty("NIPD_TracePath");
		String Trace = prop.getProperty("NIPD_Trace");
		String Delay = prop.getProperty("NIPD_Delay");
		String DelayTimeout = prop.getProperty("NIPD_DelayTimeout");
		String WaitMess = prop.getProperty("NIPD_WaitMess");
		String DCCHost = prop.getProperty("NIPD_DCCFlag");
		log.info("Finished reading configurations");
		
		JSONObject reqObj = new JSONObject();
	    try {
	    	reqObj.put("PortName", PortName);
	    	reqObj.put("Baud rate", Baudrate);
	    	reqObj.put("Timeout", Timeout);
	    	reqObj.put("TxnTimeout", TxnTimeout);
	    	reqObj.put("UploadTimeout", UploadTimeout);
	    	reqObj.put("TracePath", TracePath);
	    	reqObj.put("Trace", Trace);
	    	reqObj.put("Delay", Delay);
	    	reqObj.put("DelayTimeout", DelayTimeout);
	    	reqObj.put("WaitMess", WaitMess);
	    	reqObj.put("DCCHost", DCCHost);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	    
	    /*params.setProperties(reqObj);
	    pr.setRequest(params);*/
	    //params.setTransactionID("0001");
	    if (params.getType() == 0){
	    	log.info("inside getauth request code for stub");
	    	//auth flow
	    	params.setProperties(reqObj);
	 	    pr.setRequest(params);
	 	    log.info("amount from POS for adding payment flow is "+ params.getAmount().toString());
		    log.info("Sending request to payment device for adding payment to POS... ");
	    }else if(params.getType() == 2){
	    	log.info("inside void flow");
	    	//void flow
	    	log.info("amount from POS for voiding payment flow "+params.getAmount().toString());
	    	log.info("amount from POS for voiding payment flow multiplied by 100 "+params.getAmount().multiply(new BigDecimal(100)));
	    	log.info("amount from POS for voiding payment flow multiplied by 100 with tostring "+params.getAmount().multiply(new BigDecimal(100)).toString());
	    	log.info("amount from POS for voiding payment flow without decimals with tostring: "+new DecimalFormat("#").format(params.getAmount().multiply(new BigDecimal(100))).toString());
	    	String amountForVoid = new DecimalFormat("#").format(params.getAmount().multiply(new BigDecimal(100))).toString();
	    	log.info("Sending request to payment device to remove payment from POS... ");
	    }
	    
	   // log.info("Amount for get auth: " +params.getAmount());
	    //log.info("amount after multiplied by 100"+ params.getAmount().multiply(new BigDecimal(100)).toString());
	    
		
		JSONObject resultObj = new JSONObject();
	    try {
	    	if(params.getType() == 0){
	    		log.info("inside getauth repsonse code for stub");
	    		/*resultObj.put("cardNumber", "999******1202");
		    	resultObj.put("approvalCode", "568802");*/
	    		//setting cardlogo to the device's response
	    		if (params.getAmount().compareTo(new BigDecimal(50)) == 1){
	    			resultObj.put("cardlogo", "MASTER");
	    		}else {
	    			resultObj.put("cardlogo", "VISA");
	    		}
		    	//resultObj.put("cardlogo", "00");
		    	resultObj.put("cardNumber", "1234****7890");
		    	resultObj.put("approvalCode", "1234");
		    	resultObj.put("sequenceNumber", "26");
		    	resultObj.put("ecrReceiptNumber", "0001");
		    	pr.setProperties(resultObj);
		    	pr.setResultCode("00");
		    	pr.setResultMessage("successful adding payment process");
		    	
	    	} else if (params.getType() == 2){
	    		try {
					log.info("params in json "+params.toJSON());
					//log.info("params in string "+ params.toString());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	    		
	    		try {
					log.info("ecr from voidproperties from jsonObj " +params.getProperties().getString("ecrReceiptNumber"));
					log.info("seqno from voidproperties from jsonObj " +params.getProperties().getString("sequenceNumber"));
					log.info("amount after multiplied by 100 for void "+ params.getAmount().multiply(new BigDecimal(100)));
				} catch (Exception e1) {
					log.info("Error in multiplying amount by 100:"+e1);
					e1.printStackTrace();
				}

	    		resultObj.put("cardNumber", "999******1202");
		    	resultObj.put("approvalCode", "568802");
		    	resultObj.put("amount", "");
		    	pr.setProperties(resultObj);
		    	pr.setResultCode("00");
		    	pr.setResultMessage("successful void process");
		    	log.info("successful void process");

	    	}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (pr.getResultCode() == null){
			pr.setResultCode("200");
			pr.setResultMessage("success");
			pr.setProperties(resultObj);
		}
    
	    log.info("Exiting from executeStub");
		return pr;	
	    
	}
	
	@Override
	public PaymentResult execute(PaymentRequest params) {
		String[] args = {};
		AppConfig prop = new AppConfig(args);
		prop.load();
		log.info("NI Payment Device started");
		PaymentResult pr = new PaymentResult();
		/*if(!DLLLoader.InitDLL()){
			executeStub(params, pr);
		} else {*/
			log.info("Inside main execute method");
			try {
				log.info("params in json " +params.toJSON());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			log.info("Mashreq terminal id: " +params.getTerminalID());
			TxnInput objInput = new TxnInput();
			objInput.VFI_Amount = params.getAmount().toString();
			objInput.VFI_TransType = "1";
			Random rand = new Random();
			int eCRRcptNum = rand.nextInt(10000);
			objInput.PUTECRRcptNum = String.valueOf(eCRRcptNum);
			objInput.VFI_TID = params.getTerminalID();
			TxnOutput objOutput = new TxnOutput();
			TxnOutput voidObjOutput = new TxnOutput();
			try {

			// get the property value from config
			log.info("Reading configurations from openbravo.properties file");
			/*objInput.VFI_PortName = prop.getProperty("NIPD_PortName");
			objInput.VFI_Baudrate = prop.getProperty("NIPD_Baudrate");
			objInput.VFI_Timeout = prop.getProperty("NIPD_Timeout");
			objInput.VFI_TxnTimeout = prop.getProperty("NIPD_TxnTimeout");
			objInput.VFI_UploadTimeout = prop.getProperty("NIPD_UploadTimeout");
			objInput.VFI_TracePath = prop.getProperty("NIPD_TracePath");
			objInput.VFI_Trace = prop.getProperty("NIPD_Trace");
			objInput.VFI_Delay = prop.getProperty("NIPD_Delay");
			objInput.VFI_DelayTimeout = prop.getProperty("NIPD_DelayTimeout");
			objInput.VFI_WaitMess = prop.getProperty("NIPD_WaitMess");
			objInput.VFI_DCCHost = prop.getProperty("NIPD_DCCFlag");*/
			
			} catch (Exception e) {
				System.out.println("Exception in reading the property file: " + e);
			}  	

				pr.setRequest(params);
				log.info("Sending request to device");

				if (params.getType() == 0){
					log.info("inside get auth");
//=============================== getauth =======================================//					
					//call get auth
					// Device Input for VFI_GetAuth for Sale

					/*objInput.VFI_TransType = "01";
					objInput.VFI_ECRRcptNum = "0001";
					objInput.VFI_Amount= params.getAmount().toString();*/
					log.info("inputs for getauth: transaction type is: "+ params.getType());
					log.info("payment request amount is "+ params.getAmount().toString());
					log.info("successfully setting params for device for getauth");

					try {
					//	if(DLLLoader.InitDLL()){
							System.out.println("DLL loaded successfully, calling GetAuth");
		//					log.info("Before GetAuth, VFI_PortName: " +objInput.VFI_PortName);
							log.info("DLL loaded successfully, calling GetAuth");
							boolean a = DLLLoader.VFI_GetAuth(objInput, objOutput);
							log.info("GetAuth retuned: " +a);		
			//			}
					} catch(Exception e){
						log.info("In GetAuth error block");
						e.printStackTrace();
						log.info(e.getMessage());
					} finally {
						log.info("Response Code: " +objOutput.VFI_RespCode);
						log.info("Response Message: " +objOutput.VFI_RespMess);
						log.info("Card Number: " +objOutput.VFI_CardNum);
						log.info("Approval Code: " +objOutput.VFI_ApprovalCode);
	//					log.info("Ref No " +objOutput.VFI_RefNo);
						log.info("Trans Source " +objOutput.VFI_TransSource);
						log.info("Date Time: " +objOutput.VFI_DateTime);
						log.info("Sequence number is: "+ objOutput.VFI_MessNum);
						log.info("ecrReceiptNumber is: "+ objOutput.VFI_ECRRcptNum);
						log.info("Amount returned from device from getAuth is "+ objOutput.VFI_Amount);
					}

			if(objOutput.VFI_RespCode.isEmpty() || objOutput.VFI_RespCode == ""){
				pr.setResultCode("200");
				pr.setResultMessage("success");
			} else {
				JSONObject resultObj = new JSONObject();    
				try {
					resultObj.put("cardlogo", objOutput.VFI_CardSchemeName);
					resultObj.put("cardNumber", objOutput.VFI_CardNum);
					resultObj.put("approvalCode", objOutput.VFI_ApprovalCode);
					resultObj.put("sequenceNumber", objOutput.VFI_MessNum);
					resultObj.put("ecrReceiptNumber", objOutput.VFI_ECRRcptNum);
				} catch (JSONException e) {
					e.printStackTrace();
				}

			    pr.setProperties(resultObj);
				pr.setResultCode(objOutput.VFI_RespCode);
				pr.setResultMessage(objOutput.VFI_RespMess);
			}
				} else if (params.getType() == 2){
//=============================== void trans =======================================//
					//set input parameters
					try {
				//		objInput.VFI_ECRRcptNum = params.getProperties().getString("ecrReceiptNumber");
						objInput.PUTECRRcptNum = params.getProperties().getString("ecrReceiptNumber");
						objInput.VFI_VoidReceiptNum = params.getProperties().getString("sequenceNumber");
						String amtForVoidWithoutDecimal = new DecimalFormat("#").format(params.getAmount().multiply(new BigDecimal(100))).toString();
						objInput.VFI_Amount = amtForVoidWithoutDecimal;
						log.info("Inputs for void transaction: ");
				//		log.info("ecrReceiptNumber from POS is: " +objInput.VFI_ECRRcptNum);
						log.info("ecrReceiptNumber from POS is: " +objInput.PUTECRRcptNum);
						log.info("sequenceNumber from POS is: "+ objInput.VFI_VoidReceiptNum);
						log.info("Amount from POS to void is: "+params.getAmount().toString());
						log.info("amount send to device is: "+amtForVoidWithoutDecimal);
						log.info("successfully setting params to device for voidtrans");
					} catch (JSONException exc) {
						log.info("exception while reading input for voiding payment, "+ exc.getMessage());
						exc.printStackTrace();
					}

					// calling void trans
					try {
						log.info("calling void trans");
						boolean voidTrans = DLLLoader.VFI_VoidTrans(objInput, voidObjOutput);
						log.info("void transaction returned: "+voidTrans);
					} catch (Exception exc) {
						log.info("exception while calling voidTrans method, "+ exc.getMessage());
						exc.printStackTrace();
					} finally {
						log.info("Response Code: " +voidObjOutput.VFI_RespCode);
						log.info("Response Message: " +voidObjOutput.VFI_RespMess);
						log.info("Card Number: " +voidObjOutput.VFI_CardNum);
						log.info("Approval Code: " +voidObjOutput.VFI_ApprovalCode);
						//log.info("Ref No" +voidObjOutput.VFI_RefNo);
						log.info("Trans Source" +voidObjOutput.VFI_TransSource);
						log.info("Date Time: " +voidObjOutput.VFI_DateTime);
						log.info("Sequence number is: "+ voidObjOutput.VFI_MessNum);
						log.info("ecrReceiptNumber is: "+ voidObjOutput.VFI_ECRRcptNum);
						log.info("TID is: "+ voidObjOutput.VFI_TID);
						log.info("amount voided from device is: "+ voidObjOutput.VFI_Amount);
					}

					//setting result obj, which will be put to properties
					JSONObject voidResultObj = new JSONObject();    
					try {
						voidResultObj.put("cardNumber", voidObjOutput.VFI_CardNum);
						voidResultObj.put("approvalCode", voidObjOutput.VFI_ApprovalCode);
						voidResultObj.put("sequenceNumber", voidObjOutput.VFI_MessNum);
						voidResultObj.put("ecrReceiptNumber", voidObjOutput.VFI_ECRRcptNum);
						voidResultObj.put("amount", voidObjOutput.VFI_Amount);
					} catch (JSONException e) {
						log.info("exception while reading output of void trans, "+ e.getMessage());
						e.printStackTrace();
					}

					//setting parameters to return
					try {
						log.info("setting response for void");
						pr.setProperties(voidResultObj);
						pr.setResultCode(voidObjOutput.VFI_RespCode);
						pr.setResultMessage(voidObjOutput.VFI_RespMess);
					} catch (Exception exc) {
						log.info("exception while reading response code and response message, "+ exc.getMessage());
						exc.printStackTrace();
					}
				//}
		}
		return pr;
	}

	@Override
	public JComponent getComponent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return "NI Payment Integration";
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "NI Payment Device";
	}
}